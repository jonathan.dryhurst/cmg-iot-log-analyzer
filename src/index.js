const fs = require('fs');
const es = require('event-stream');
const HUMDITY_SENSOR_LIMIT = 1;

function calculateMean(values) {
  return (values.reduce((a, b) => a + b, 0) / values.length).toFixed(1);
}

function calculateStandardDeviation(values, mean) {
  const aveSqrDiff = values.map(value => {
    return Math.pow(value - mean, 2);
  });
  return Math.sqrt(calculateMean(aveSqrDiff));
}

function isBad(reference, values) {
  percentageDifference = values.map(value => {
    return 100 * Math.abs((reference - value) / ((reference + value) / 2));
  });
  return percentageDifference.some(humidityPercentageOverLimit);
}

function humidityPercentageOverLimit(value) {
  return value > HUMDITY_SENSOR_LIMIT;
}

function determineQuality(values, type, name, reference) {
  const mean = calculateMean(values);
  const standardDeviation = calculateStandardDeviation(values, mean);
  switch (type) {
    case 'thermometer': {
      if (Math.abs(reference - mean) <= 0.5 && standardDeviation < 5)
        return 'very precise';
      else if (Math.abs(reference - mean) <= 0.5 && standardDeviation < 3)
        return 'ultra precise';
      else return 'precise';
    }
    case 'humidity': {
      return isBad(reference, values) ? 'discard' : 'keep';
    }
  }
}

module.exports = fileContents =>
  new Promise((resolve, reject) => {
    let curName = null;
    let curType = null;
    let devices = [];
    let humidity = null;
    let numLines = 0;
    let reference = null;
    let results = {};
    let temperature = null;

    const stream = fs
      .createReadStream(fileContents)
      .pipe(es.split())
      .pipe(
        es.mapSync(line => {
          curLine = line.split(' ');
          if (numLines === 0) {
            temperature = Number(curLine[1]);
            humidity = Number(curLine[2]);
          } else {
            if (!Date.parse(curLine[0])) {
              const type = curLine[0];
              const name = curLine[1];
              if (curType == 'thermometer') reference = temperature;
              if (curType == 'humidity') reference = humidity;
              if (curName !== null) {
                results[curName] = determineQuality(
                  devices[curType][curName],
                  curType,
                  curName,
                  reference
                );
                devices = [];
              }
              !devices[type] ? (devices[type] = []) : null;
              devices[type][name] = [];
              curType = type;
              curName = name;
            } else {
              devices[curType][curName].push(Number(curLine[1]));
            }
          }
          numLines++;
        })
      )
      .on('end', () => {
        results[curName] = determineQuality(
          devices[curType][curName],
          curType,
          curName,
          reference
        );
        devices = [];
        resolve(results);
      });
  });
