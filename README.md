# cmg-iot-log-analyzer

A JavaScript module that reads IoT log files then returns an object of
devices and their corresponding quality statuses.

The module is device agnostic meaning you could feed it any <type><name>
combination and it shouldn't matter; however, the module would need to
be amended to include quality assurance logic for the new device(s).

It is also date format agnostic as long as the date format makes sense.

Compatibility: This module was written using Node.js v10.15.3 LTS and
has not been tested against earlier versions.

## Installation

with [npm](https://www.npmjs.org) installed, run

```sh
$ npm install
```

## Example

An example exists in the examples directory:

```
const evaluateLogFile = require('..');
const sampleData = __dirname + '/data/sensors.log';

evaluateLogFile(sampleData).then(result => console.log(result));
```

## Todo

Write tests.

## License

ISC

## Keywords

none
